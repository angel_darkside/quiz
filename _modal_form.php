<div class="fade modal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">New User</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
            <form   action="save.php" id="form_add" method="post" data-validate="parsley">
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-6">
                        <label for="recipient-name" class="col-form-label">First Name *</label>
                        <input type="text" class="form-control" name="first_name" data-parsley-trigger="change focusout"  data-parsley-maxlength="255" required="required" data-parsley-required-message="This field is required">
                     </div>
                     <div class="col-md-6">
                        <label for="recipient-name" class="col-form-label">Last Name *:</label>
                        <input type="text" class="form-control" name="last_name" data-parsley-trigger="change focusout"  data-parsley-maxlength="255" required="required" >
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-6">
                        <label for="gender">Gender *</label>
                        <p>
                           Male: <input type="radio" name="gender"   id="genderM" value="1" required="">
                           Female: <input type="radio" name="gender" id="genderF" value="2" required="">
                        </p>
                     </div>
                     <div class="col-md-6">
                        <label for="date">Date of Birth *</label>
                        <input id="date" type="text" name ="birthday" class="form-control date_picker" data-parsley-trigger="change focusout"  required="required" />
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <div class="row">
                     <div class="col-md-6">
                        <label for="email">Email *</label>
                        <input id="email" type="email" name ="email" class="form-control"  data-parsley-maxlength="150" data-parsley-trigger="change focusout"  required="required"  />
                     </div>
                     <div class="col-md-6">
                        <label for="email">Phone *</label>
                        <input id="phone" type="text" name ="phone" data-parsley-pattern="^[0-9][0-9]*$"  data-parsley-maxlength="15" class="form-control" data-parsley-trigger="change focusout"  required="required"  />
                     </div>
                  </div>
               </div>
               <div class="form-group">
                  <label for="message-text" class="col-form-label">Adress:</label>
                  <textarea class="form-control" name="address" id="message-text"></textarea>
               </div>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary"  data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-primary" data-rowIndex="" data-id="" data-action="" id="submit">Submit</button>
         </div>
      </div>
   </div>
</div>