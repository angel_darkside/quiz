<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>Test </title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" type="text/css" href="css/daterangepicker.css">
      <link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.css">
      <link rel="stylesheet" type="text/css" href="css/parsley.css">
      <style type="text/css">
         .alert_error{
         display: none;
         border: 1px solid #ff8080;
         }
         .alert_success{
         display: none;
         }
         .alert_success ul.success {
         list-style: none;
         background-color: #d6e9c6;
         border-color: #d6e9c6;
         color: #3c763d;
         margin:0 auto;
         margin-bottom: 20px;
         }
         .alert_error ul.error  {
         list-style: none;
         color: #ff8080;
         margin:0 auto;
         }
         .alert_error ul.error  li {
         text-align: left;
         }
         /* parsely */
         input.parsley-error, select.parsley-error, textarea.parsley-error {
         border: 1px solid #ff8080;
         }
         ul.parsley-errors-list {
         list-style: none;
         color: #ff8080;
         padding-left: 5px;
         margin-top: 5px;
         }
         input.parsley-error, select.parsley-error, textarea.parsley-error {
         border: 1px solid #ff8080;
         }
      </style>
   </head>