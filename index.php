<?php
   require 'connect.php';
   $sql = "select * from users order by id asc";
   $result = mysqli_query($conn, $sql);
   ?>
<!DOCTYPE html>
<?php require '_header.php' ?>
<body class="">
   <div class="main-website container-fluid" id="app" >
   <div class="content-section row">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <h1> All User <button type="button" class="btn btn-primary" data-action="add">Add new user</button></h1>
               <table class="table">
                  <thead>
                     <tr>
                        <th scope="col"></th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Birthday</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Address</th>
                        <th scope="col">Action</th>
                     </tr>
                  </thead>
                  <tbody id="data">
                     <?php $i= 1;
                        while($row = mysqli_fetch_array($result)) {
                        
                           $gender = ($row['gender']== 1) ? "Male" : "Female";
                          ?>
                     <tr>
                        <td><?php echo $i ?></td>
                        <td><?php echo $row['first_name'] ?></td>
                        <td><?php echo $row['last_name'] ?></td>
                        <td><?php echo $gender ?></td>
                        <td><?php echo $row['birthday']?></td>
                        <td><?php echo $row['email']?></td>
                        <td><?php echo $row['phone']?></td>
                        <td><?php echo $row['address']?></td>
                        <td>
                           <button type="button" class="btn btn-default action"  data-action="edit" data-id="<?php echo $row['id']?>">Edit</button>
                           <button type="button" class="btn btn-danger"   data-action="delete" data-id="<?php echo $row['id']?>">Delete</button>
                        </td>
                     </tr>
                     <?php $i++;
                        }
                        ?>
                  </tbody>
               </table>
            </div>
         </div>
      </div>
      <?php require '_modal_form.php' ?>
   </div>
   <script src="js/jquery-2.2.2.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   <script type="text/javascript" src="js/moment.min.js"></script>
   <script type="text/javascript" src="js/daterangepicker.js"></script>
   <script type="text/javascript" src="js/bootstrap-datetimepicker.min.js"></script>
   <script type="text/javascript" src="js/parsley.js"></script>
   <script type="text/javascript">
      $(document).ready(function(){
      $('.date_picker').datetimepicker({
        format:'YYYY-MM-DD',
        viewMode: "years",
        maxDate:moment().format('LLLL')
      }).on('keypress', function (e) {
        e.preventDefault();
        return false;
      });
        $('.modal').on('hidden.bs.modal', function(){
      
            action = $(this).find("#submit").data('action');
            if(action == 'add'){
             $(this).find('form')[0].reset();
             $('#form_add').parsley('reset');
            }
        });
      
          $(document).on('click','.btn',function(){
                   var action = $(this).data('action');
                   var id = $(this).data('id');
                   var rowIndex = $(this).parent().parent().index();                  
                   switch(action){
                    case 'add' :
                        $('#submit').data('action','add');
                        $('.modal').modal('show');
                        break;
                    case 'edit' :
                        editRow(id, rowIndex);
                        break;
                    case 'delete' :
                        deleteRow(id ,rowIndex);
                        break;
                    default:
                         $('.modal').modal('show');
                   }
             })
      
      $('#submit').click(function(){
            var action = $(this).data('action');
            validate = $('#form_add').parsley().validate();
            id= $(this).data('id');
            rowIndex = $(this).data('rowIndex');
            data = $("#form_add").serialize()+ "&action="+action+"&id="+id;
           if(validate){
            var first_name = $("input[name=first_name]").val();
            var last_name = $("input[name=last_name]").val();
            var gender = null;
            if($('#genderM').is(':checked')){
              gender = 1;
      
            }else  if($('#genderF').is(':checked')){
              gender = 2
            }
            var birthday = $("input[name=birthday]").val();
            var email = $("input[name=email]").val();
            var phone = $("input[name=phone]").val();
            var address = $("textarea[name=address]").val();
            var count = $("#data tr").length;
              jQuery.ajax({
                      url: "manage.php",
                      type:"POST",
                      data: data,
                      dataType:"JSON",
                      success:function(ret){
                          console.log(ret);
                          var txt_gender = '';
                          if(gender == 1){
                            txt_gender = 'Male';
                          }else if(gender == 2){
                             txt_gender = 'Female';
                          }
                            count += 1;
                          var trOjb = '<tr>'+
                                          '<td scope="row">'+count+'</td>'+
                                          '<td scope="row">'+first_name+'</td>'+
                                          '<td scope="row">'+last_name+'</td>'+
                                          '<td scope="row">'+txt_gender+'</td>'+
                                          '<td scope="row">'+birthday+'</td>'+
                                          '<td scope="row">'+email+'</td>'+
                                          '<td scope="row">'+phone+'</td>'+
                                          '<td scope="row">'+address+'</td>'+
                                          '<td scope="row">'+
                                          '<button type="button" class="btn btn-default" data-action="edit" data-id="'+ret['id']+'">Edit</button> '+
                                          '<button type="button" class="btn btn-danger" data-action="delete" data-id="'+ret['id']+'">Delete</button></td>'+
                                          '</tr>'
                        if(ret['result']){
                          if(action == 'add'){
                                 
                              $('#data').append(trOjb);
                          }else if(action == 'update'){
                                  var rows = $('#data tr');
                                  rows.eq(rowIndex).replaceWith(trOjb);
                            
                              }
                               $('.modal').modal('toggle');
                          }
                        console.log(ret);
                   }  
              });
          }
       });
      
      });
      
      function editRow(id ,rowIndex){
           $.ajax({
                      url: "manage.php",
                      type:"POST",
                      data: {"id": id, "action" : "edit"},
                      dataType:"JSON",
                      success:function(ret){
                       
                        $("input[name=first_name]").val(ret['first_name']);
                        $("input[name=last_name]").val(ret['last_name']);
                        $("input[name=birthday]").val(ret['birthday']);
                        $("input[name=email]").val(ret['email']);
                        $("input[name=phone]").val(ret['phone']);
                        $("textarea[name=address]").val(ret['address']);
                        $('#submit').data('action','update');
                        $('#submit').data('id',id);
                        $('#submit').data('rowIndex',rowIndex);
                        $('#form_add').parsley().reset();
                        $('.modal').modal('show');
                         if(ret['gender'] == 1){
                              document.getElementById("genderM").checked = true;
                          }else if(ret['gender'] == 2){
                               document.getElementById("genderF").checked = true;
                          }
                       
                        console.log(ret);
                   }  
              });
      
      }//end
      
      function deleteRow(id ,rowIndex){
           $.ajax({
                      url: "manage.php",
                      type:"POST",
                      data: {"id": id, "action" : "delete"},
                      dataType:"JSON",
                      success:function(ret){
                        if(ret['result']){
                         $('#data tr').eq(rowIndex).remove();                              
                        }
                        console.log(ret);
                   }  
              });
      
      }
   </script>
</body>
</html>